import React from 'react';
import Form from 'react-bootstrap/Form';

class CompanyInformation extends React.Component {
	constructor(props) {
    super(props);
    this.state = {orgNr: '',
									Namn: '',
									Epost: '',
									questionList: []};

		this.handleChangeOrgNr = this.handleChangeOrgNr.bind(this);
		this.handleChangeNamn = this.handleChangeNamn.bind(this);
		this.handleChangeEpost = this.handleChangeEpost.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleChangeCompanyType = this.handleChangeCompanyType.bind(this);
  }

	// Laddar en initial questionlist då ingen change har hänt i dropdown.
	componentDidMount() {
		this.handleChangeCompanyType({target: {value: "1"}});
	}

	// Sparar organisationsnummer i state.
  handleChangeOrgNr(event) {
    this.setState({orgNr: event.target.value});
  }

	// Sparar Namn i state.
	handleChangeNamn(event) {
		this.setState({Namn: event.target.value});
	}

	// Sparar E-post i state.
	handleChangeEpost(event) {
		this.setState({Epost: event.target.value});
	}

	// Sparar all företagsinfo samt vald frågeserie i Parent via callback.
  handleSubmit(event) {
    this.props.registerCompanyInfo(this.state);
  }

	// Laddar en questionList baserat på val i dropdown, företagstyp.
	// Sparar vald questionlist i state.
	handleChangeCompanyType(event) {
		let list = [];
		if (event.target.value === "1") {
			list = require("./json/smallCompany.json");
		} else if (event.target.value === "2") {
			list = require("./json/mediumCompany.json");
		} else if (event.target.value === "3") {
			list = require("./json/largeCompany.json");
		}

		this.setState({questionList: list});
	}

  render() {
    return (

      <div>

				<h1 className="welcomeText">Undersökning för <strong>Innovation Due Diligence</strong></h1>

				<div className="p-2 w-25">
				<p className="small font-italic">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
				</p>
				</div>

				<form onSubmit={this.handleSubmit}>

					<div className="form-group">
						<label className="lable col-2"> Org.Nr: </label>

						<div className="col-10">
							<input class="form-control" type="text" value={this.state.orgNr} onChange={this.handleChangeOrgNr}  />
						</div>

					</div>

					<Form.Group controlId="formGridState">
						<Form.Label className="lable col-2">Typ av företag</Form.Label>
						<Form.Control as="select" onChange={this.handleChangeCompanyType}>
							<option value="1">Litet företag (0-10 anställda)</option>
							<option value="2">Mellanstort företag (11-50 anställda)</option>
							<option value="3">Stort företag (>50 anställda)</option>
						</Form.Control>
					</Form.Group>

					<div className="form-group">
							<label className="lable col-2"> Namn: </label>

							<div className="col-10">
								<input className="form-control" type="text" value={this.state.Namn} onChange={this.handleChangeNamn} />
							</div>
					</div>

					<div className="form-group">

						<label className="lable col-2"> E-post:</label>

						<div className="col-10">
							<input class="form-control" type="text" value={this.state.Epost} onChange={this.handleChangeEpost}  />
						</div>
					</div>

					<div className="Start">
						<input class="form-control" type="submit" className="btn btn-primary" value="Starta undersökningen" />

					</div>
					</form>

				</div>



    );
  }
}

export default CompanyInformation;
