import React from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar'

class Questions extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			questionList: [],
			focusAreas: [],
			currentFocusArea: 0
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleBack = this.handleBack.bind(this);
	}

	// Körs när komponenten laddats.
	componentDidMount() {
		// Läser in alla unika kategorier (fokusområden) till en array för
		// användning både i statusfältet (progress) och rendering.
		// Sparas till state.
		const categories = [];
		this.props.questionList.map(questions => {
			if (categories.indexOf(questions.categoryID) === -1) {
				categories.push(questions.categoryID)
			}
		});

		this.setState({
			focusAreas: categories,
			questionList: this.props.questionList
		});
	}

	// Renderar nästa fokusområde med frågor, alternativt avslutar Questions
	// komponenten via callback till parent
	handleSubmit() {
		let questions = this.state.questionList;
		let med = 'Du måste fylla i ett svar på alla frågor för att kunna gå vidare';

		// Gå igenom alla frågor och kolla om någon är obesvarad för det aktuella fokusområdet
		if (this.state.currentFocusArea < this.state.focusAreas.length) {
			let questionIsNotAnswered = false;
			let focusAreaString = this.state.focusAreas[this.state.currentFocusArea]
			for (let i = 0; i < questions.length; i++) {
				if (focusAreaString === questions[i].categoryID && questions[i].providedAnswer < 0) {
					questionIsNotAnswered = true;
				}
			}
			// Visa meddelande om det behövs, annars gå vidare till nästa steg
			if (questionIsNotAnswered) {
				alert(med);
			} else {
				this.setState({ currentFocusArea: this.state.currentFocusArea + 1 })
			}
		}

		if (this.state.currentFocusArea === this.state.focusAreas.length) {
			this.props.endQuestions(this.state.questionList)
		}
	}

	// Backar till föregående fokusområde med frågor.
	// komponenten via callback till parent
	handleBack() {
		if (this.state.currentFocusArea !== 0) {
			this.setState({ currentFocusArea: this.state.currentFocusArea - 1 })
		}
	}

	// Funktion för att göra radio-buttons till controlled components. Uppdaterar
	// konstant state variabler baserat på valt alternativ. Behöver inte vänta
	// submit utan uppdaterar och renderar checked vid förändring.
	handleChange(event) {
		let questions = this.state.questionList;
		let index = questions.findIndex((x) => x.questionID == event.target.id);
		questions[index].providedAnswer = event.target.value;
		this.setState({ questionList: questions });
	}

	// Rendering av fråga. Skapar text och radiobuttons.
	renderQuestion(question) {
		return (
			<form key={question.questionID}>

				<div className="container w-75 QuestionX">

					<div className="row">

						<div className="Question col-8 text-left xQuestions">

							<h4>{question.title}</h4>
							<p>{question.description}</p>

						</div>

						<div className="col- xAnswers">

							<label className="radio-inline"><input className="radioB" type="radio" name="optradio" value={question.answer1_score} checked={question.providedAnswer == question.answer1_score} onChange={this.handleChange} id={question.questionID} /> <p>1</p> </label>

							<label className="radio-inline"><input className="radioB" type="radio" name="optradio" value={question.answer2_score} checked={question.providedAnswer == question.answer2_score} onChange={this.handleChange} id={question.questionID} /> <p>2</p> </label>

							<label className="radio-inline"><input className="radioB" type="radio" name="optradio" value={question.answer3_score} checked={question.providedAnswer == question.answer3_score} onChange={this.handleChange} id={question.questionID} /> <p>3</p> </label>

							<label className="radio-inline"><input className="radioB" type="radio" name="optradio" value={question.answer4_score} checked={question.providedAnswer == question.answer4_score} onChange={this.handleChange} id={question.questionID} /> <p>4</p> </label>

							<label className="radio-inline"><input className="radioB" type="radio" name="optradio" value={question.answer5_score} checked={question.providedAnswer == question.answer5_score} onChange={this.handleChange} id={question.questionID} /> <p>5</p> </label>


						</div>

					</div>

				</div>

			</form>);
	}

	// Renderar fokusområde, kallar i sin tur på renderQuestion vilket Renderar
	// alla frågor inom aktuellt fokusområde.
	renderFocusArea() {
		if (this.state.currentFocusArea === this.state.focusAreas.length) {
			return <div>
				<p><strong>Tack för din medverkan!</strong></p>
				<p>Gå vidare för att se resultatet</p>
			</div>
		}

		return (this.state.questionList
			.filter((e) => e.categoryID === this.state.focusAreas[this.state.currentFocusArea])
			.map((question) => {
				return (this.renderQuestion(question));
			}));
	}

	// Renderar progressbar samt fokusområde innehållande alla relevanta frågor.
	render() {
		let done = this.state.currentFocusArea === this.state.focusAreas.length
		return (
			<div>
				<ProgressBar
					min={0}
					max={this.state.focusAreas.length}
					now={this.state.currentFocusArea}
					label={!done ? `${this.state.currentFocusArea + 1}/${this.state.focusAreas.length}` : "Klar"} />
				{this.state.focusAreas[this.state.currentFocusArea]}
				{this.renderFocusArea()}
				<div className="SubmitButton">
					<button className="backButton" onClick={this.handleBack}>
						Föregående
				</button>
					<button className="nextButton" onClick={this.handleSubmit}>
						Nästa
				</button>
				</div>


			</div>
		);
	}
}


export default Questions;
