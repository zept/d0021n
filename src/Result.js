import React from 'react';
import Radar from 'react-d3-radar';
import Questions from './Questions';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

class Result extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: [],
      focusAreas: [],
      chartData: {},
    };
    this.displayChart = this.displayChart.bind(this);
    this.mapAnswers = this.mapAnswers.bind(this);
  }

  // Körs när komponenten laddats.
	componentDidMount() {
		// Läser in alla unika kategorier (fokusområden) till en array för
		// användning både i statusfältet (progress) och rendering.
		// Sparas till state.
		const categories = [];
		this.props.answers.map(answer => {
			if (categories.indexOf(answer.categoryID) === -1) {
				categories.push(answer.categoryID)
			}
		});

		this.setState({
			focusAreas: categories,
			answers: this.props.answers
		}, () => this.mapAnswers());
	}

  mapAnswers() {

    // Create basic object structure.
    let dataObject = {variables: [],
                      sets: [{key: 'Resultat',
                              label: 'Resultat',
                              values: {}}]};

    // Iterate the focus areas.
    this.state.focusAreas.map(focusArea => {
      dataObject.variables.push({key:focusArea,
                                  label: focusArea})

      // Base scare and number of questions
      let score = 0.0;
      let numberOfQuestions = 0;

      // Iterate the questions and check the focusArea
      this.state.answers.map(answer => {
        if (answer.categoryID === focusArea) {

          // If it's the right focusArea, add providedScore to total
          score = score + parseFloat(answer.providedAnswer);
          numberOfQuestions = numberOfQuestions + 1;
        }
      });

      // Calculate percentage of questions in current focusArea and append to object
      // Hardcoded max-value per question, add check for max possible value to make dynamic.
      let percentageScore = Math.round((score / (numberOfQuestions*5))*100);
      dataObject.sets[0].values[focusArea] = percentageScore;
    })

    // Save the data object into state.
    this.setState({chartData: dataObject})
  }

  displayChart() {
    // Check if there is a populated data object.
      if (this.state.chartData.variables !== undefined) {
        return(
          <div className="w-25">
                                <Radar
                                      width={300}
                                      height={300}
                                      padding={50}
                                      domainMax={100}
                                      highlighted={null}
                                      data={this.state.chartData}
    />
          </div>);
      }
    }


  render() {

    return (

			<div className="Result">
        <h4> Du har nu genomfört undersökningen erbjuden av <strong> Innovation Due Diligence</strong> </h4>
        <p>För frågor, vidare analys eller hjälp att tolka resultatet kontakta oss via info@innovationduediligence.se. <br/> Alternativt ange din e-postadress nedan och klicka på 'Kontakta mig'</p>

          {this.displayChart()}

          <div className="w-25 align-center">
            <InputGroup>
              <FormControl
                placeholder="E-post adress"
                aria-label="E-post adress"
                aria-describedby="basic-addon2"
                className="email"
              />
                <InputGroup.Append>
                  <Button className="ml-3 mr-2" variant="btn-primary btn-sm result-btn">Skicka resultat</Button>
                  <Button className="mx-2" variant="btn-primary btn-sm result-btn">Kontakta mig</Button>
              </InputGroup.Append>
            </InputGroup>
            </div>
  			</div>

    );
  }
}

export default Result;
