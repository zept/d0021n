import React from 'react';
import ReactDOM from 'react-dom';
import CompanyInformation from './CompanyInformation';
import Questions from './Questions';
import Result from './Result';
import HeaderBar from './HeaderBar';


class MainView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      companyInformation: {},
      progress: 0,
    };
  }

  // Callback som körs från child component för att spara bolagsinformation
  // samt fortsätta till nästa del i undersökningen.
  handleCompanyInformation = (companyinfo) => {
    this.setState({companyInformation: companyinfo,
                    questions: companyinfo.questionList,
                    progress: 1});
  }

  // Callback som körs från child component för att spara svarsinformation
  // samt fortsätta till nästa del i undersökningen.
  handleAnswers = (answers) => {
    this.setState({answers: answers,
                    progress: 2});
  }

  // Håller koll på var i undersökningen användaren är samt renderar relevant
  // komponent.
  progress() {
    if (this.state.progress == 0) {
      return <CompanyInformation registerCompanyInfo={this.handleCompanyInformation} />;
    } else if (this.state.progress == 1) {
      return <Questions questionList={this.state.questions} endQuestions={this.handleAnswers}/>;
    } else if (this.state.progress == 2) {
      return <Result answers={this.state.answers}/>;
    }
  }


  // Rendering.
  render() {
    return (
      <div className="root">

        <HeaderBar> </HeaderBar>

        <div className="searchbar" align="center">

              {this.progress()}

        </div>

      </div>
    );
  }
}

export default MainView;

