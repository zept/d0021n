import React from 'react';
import logo1 from './img/logo1.png';
import logo2 from './img/logo2.png';
import logo3 from './img/logo3.png';

class MainHeader extends React.Component{


  //Popup ruta för inte råka klicka fel
  handleClickOnStart() {

      if (window.confirm("Tillbaka till startsida?")) {

        window.location.href = "./MainView.js";
      } 

	}

  render() {
    return (
      <div>
          <div className="mainHeader fixed-top">

                  <div className="top-logo-container">

                    <a target="_blank" href="https://www.ltu.se/org/LTU-Holding/LTU-Business"> <img src={logo1} alt="Logo1" /></a>
                    <a target="_blank" href="https://www.vinnova.se/"> <img src={logo2} alt="Logo2" /></a>
                    <a target="_blank" href="https://www.sipstrim.se/sv/hem/"> <img src={logo3} alt="Logo3" /></a>
                  
                  </div>

                  <nav class="NavbarLinks navbar justify-content-end">
                    <a class="navbarlink navbar-brand" href="#" onClick={this.handleClickOnStart}><p><b>Start</b></p></a>
                    <a class="navbarlink navbar-brand" href="#"><p><b>Learning</b></p></a>
                  </nav>     

          </div>

      </div>

    );
  }
}

export default MainHeader;