import React from 'react';
import ReactDOM from 'react-dom';
import MainView from './mainview'
import './css/bootstrap.css';
import './css/styles.css';

// Läs in applikationen, placera i index.html root.
ReactDOM.render(
	<MainView />,
	document.getElementById('root')
);
